﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    class SceneCreator
    {
        public static Model CreatePyramid(IBsdf bsdf)
        {
            
            Vertex[] vertices =  { 
                new Vertex(new Point3D(0, 0, 0), 
                    new int[] {0, 1, 2}), 
                new Vertex(new Point3D(1, 0, 0), 
                    new int[] {0, 2, 3}), 
                new Vertex(new Point3D(0, 1, 0), 
                    new int[] {0, 1, 3}), 
                new Vertex(new Point3D(0, 0, 1 ), 
                    new int[] {1, 2, 3}) 
            };

            Triplet[] triplets = {
                new Triplet(2, 1, 0),
                new Triplet(3, 2, 0),
                new Triplet(1, 3, 0),
                new Triplet(1, 2, 3)
            };
            Mesh mesh = new Mesh(vertices, triplets);
            Model model = new Model(mesh, null);
            return model;
        }
        public static Model CreateCube(MaterialOfModel material)
        {
            Vertex[] vertices = {
                new Vertex(new Point3D(-1, -1, -1),  
                    new int[] {0, 1, 2}), 
                new Vertex(new Point3D(-1, -1, 1),  
                    new int[] {6,4,3,2,1,8}), 
                new Vertex(new Point3D(-1, 1, -1),  
                    new int[] {1, 8, 7, 10, 11, 0}), 
                new Vertex(new Point3D(-1, 1, 1),  
                    new int[] {6, 7, 8}),
                new Vertex(new Point3D(1, -1, -1), 
                    new int[] {9, 5, 3, 2, 0, 11}),
                new Vertex(new Point3D(1, -1, 1), 
                    new int[] {3, 4, 5}),
                new Vertex(new Point3D(1, 1, -1),
                    new int[] {9, 10, 11}),
                new Vertex(new Point3D(1, 1, 1), 
                    new int[] {4, 6, 7, 10, 9, 5})

            };
            Triplet[] triplets = {
                new Triplet(0, 2, 4),
                new Triplet(1, 2, 0), 
                new Triplet(4, 1, 0),

                new Triplet(4, 5, 1),
                new Triplet(5, 7, 1),
                new Triplet(4, 7, 5),

                new Triplet(7, 3, 1),
                new Triplet(2, 3, 7),
                new Triplet(1, 3, 2),

                new Triplet(4, 6, 7),
                new Triplet(6, 2, 7),
                new Triplet(4, 2, 6),
            };

            return new Model(new Mesh(vertices, triplets), 
                material);
        }

        private static PerspCamera CreateCamera()
        {
//            ProjCamera camera = new ProjCamera(ProjCamera.LookAtRH(new Point3D(2, 2, 5), new Point3D(0, 0, 0), new Vector3D(0, 1, 0)),
//                ProjCamera.FrustumRH(-3, 3, -3, 3, -0.5, -3));
            PerspCamera camera = new PerspCamera();
            camera.Position = new Point3D(0, 0, 18); Point3D lookPoint = new Point3D(0, 0, 0);
            camera.LookDirection = Point3D.Subtract(lookPoint, camera.Position);
            camera.UpDirection = new Vector3D(0, 1, 0);
            camera.ZNear = -0.5; camera.ZFar = -10;
            camera.FieldOfViewX = 2 * Math.PI / 3 ;
            return camera;
        }
        private static LightSource CreateLightSource(Point3D position, Power power)
        {
            LightSource source = new PointLightSource(power, position, new Vector3D(0, -1, 0), Math.PI/24); 
            return source;
        }
        public static Scene CreateScene()
        {
            Scene scene = new Scene();
            
            scene.Camera = CreateCamera();
            LightSource source2 = CreateLightSource(new Point3D(0, 9.7, 0), new Power(Colors.White, 1000)); //new LightSource { Position = new Point3D(0, 9.7, 0), PowerOfSource = new Power(1444, 1000, 1000) };
            LightSource source1 = CreateLightSource(new Point3D(0, 0, 0), new Power(Colors.Yellow, 900));  
            Model pyramid1 = CreatePyramid(new BlinnPhongBsdf(Colors.Red, Colors.White, 100f));
            Matrix3D matrOne = Matrix3D.Identity;
            matrOne.Scale(new Vector3D(5, 5, 5));
            matrOne.Rotate(new Quaternion(new Vector3D(0, 0, 1), -180));
            matrOne.Rotate(new Quaternion(new Vector3D(0, 1, 0), -90));
            matrOne.Translate(new Vector3D(-1, -1, -1));
            pyramid1.TransformMatrix = Matrix3D.Multiply(pyramid1.TransformMatrix, matrOne);
//--------------------------------------------------------------------------------------------------
            Model pyramid = CreatePyramid(new LambertianBsdf(Colors.Yellow));
            Matrix3D matrTwo = Matrix3D.Identity; 
            matrTwo.Scale(new Vector3D(5, 5, 5));
            matrTwo.Rotate(new Quaternion(new Vector3D(0, 1, 0), -60));
            matrTwo.Rotate(new Quaternion(new Vector3D(1, 0, 0), -10));
            matrTwo.Translate(new Vector3D(5, 2, -5));
            pyramid.TransformMatrix = Matrix3D.Multiply(pyramid.TransformMatrix, matrTwo);
//--------------------------------------------------------------------------------------------------
            Model backSide = CreateCube(new MaterialOfModel(0.8f, 0.1f, Colors.Green, Colors.White, float.PositiveInfinity)); backSide.Description = "Back side";
            Matrix3D sideMatr = backSide.TransformMatrix; sideMatr.Scale(new Vector3D(10, 10, 0.25)); sideMatr.Translate(new Vector3D(0, 0, -10));
            backSide.TransformMatrix = Matrix3D.Multiply(backSide.TransformMatrix, sideMatr);
            Model topSide = CreateCube(new MaterialOfModel(0.99f, 0.01f, Colors.Red, Colors.White, 100)); topSide.Description = "Top side";
            sideMatr = topSide.TransformMatrix; sideMatr.Scale(new Vector3D(10, 0.25, 10)); sideMatr.Translate(new Vector3D(0, 10, 0));
            topSide.TransformMatrix = Matrix3D.Multiply(topSide.TransformMatrix, sideMatr);
            Model downSide = CreateCube(new MaterialOfModel(0.5f, 0.4f, Colors.Yellow, Colors.White, float.PositiveInfinity)); downSide.Description = "Down side";
            sideMatr = downSide.TransformMatrix; sideMatr.Scale(new Vector3D(10, 0.25, 10)); sideMatr.Translate(new Vector3D(0, -10, 0));
            downSide.TransformMatrix = Matrix3D.Multiply(downSide.TransformMatrix, sideMatr);
            Model rightSide = CreateCube(new MaterialOfModel(0.99f, 0.01f, Colors.MediumBlue, Colors.White, 500)); rightSide.Description = "Right side";
            sideMatr = rightSide.TransformMatrix; sideMatr.Scale(new Vector3D(0.25, 10, 10)); sideMatr.Translate(new Vector3D(10, 0, 0));
            rightSide.TransformMatrix = Matrix3D.Multiply(rightSide.TransformMatrix, sideMatr);
            Model leftSide = CreateCube(new MaterialOfModel(0.2f, 0.8f, Colors.Silver, Colors.White, float.PositiveInfinity)); leftSide.Description = "Left side";
            sideMatr = leftSide.TransformMatrix; sideMatr.Scale(new Vector3D(0.25, 10, 10)); sideMatr.Translate(new Vector3D(-10, 0, 0));
            leftSide.TransformMatrix = Matrix3D.Multiply(leftSide.TransformMatrix, sideMatr);
            Model cube = CreateCube(new MaterialOfModel(0.99f, 0.01f, Colors.Purple, Colors.Yellow, 1000)); cube.Description = "Cube";
            sideMatr = cube.TransformMatrix; sideMatr.Translate(new Vector3D(-5, -5, 5));
            cube.TransformMatrix = Matrix3D.Multiply(cube.TransformMatrix, sideMatr);
            Model prism = CreateCube(new MaterialOfModel(0.99f, 0.01f, Colors.Green, Colors.Yellow, 0)); prism.Description = "Prism";
            sideMatr = prism.TransformMatrix; sideMatr.Scale(new Vector3D(2, 4, 1));
            sideMatr.Translate(new Vector3D(5, -5.75, 3));
            prism.TransformMatrix = Matrix3D.Multiply(prism.TransformMatrix, sideMatr);
            //scene.Models.Add(pyramid);
            //scene.Models.Add(pyramid1);
            scene.Models.Add(backSide);
            scene.Models.Add(topSide);
            scene.Models.Add(downSide);
            scene.Models.Add(rightSide);
            scene.Models.Add(leftSide);
            scene.Models.Add(cube);
            scene.Models.Add(prism);
            scene.LightSources.Add(source2);
            scene.LightSources.Add(source1);
            return scene;
        }
    }
}
