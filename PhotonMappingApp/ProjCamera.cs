﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public class ProjCamera
    {
        public ProjCamera(Matrix3D viewMatr = new Matrix3D(), Matrix3D projMatr = new Matrix3D())
        {
            ViewMatr = viewMatr;
            ProjMatr = projMatr;
            TransformMatrix = Matrix3D.Identity;
        }
        public virtual Matrix3D ViewMatr { get; private set; }
        public virtual Matrix3D ProjMatr { get; private set; }
        public virtual Matrix3D TransformMatrix { get; set; }
        public static Matrix3D FrustumRH(double left, double right, double bottom, double top, double near, double far)
        {
            double width = right - left, height = top - bottom, depth = near - far;
            
            Matrix3D matr = new Matrix3D(
               2*near/width, 0,         0,               0,
               0,         2*near/height, 0,               0,
               0,         0,         far/depth,       -1,
               0,         0,         far*near/depth, 0
            );
            /*
            Matrix3D matr = new Matrix3D(
               2*near/sW, 0,         0,               0,
               0,         2*near/height, 0,               0,
               0,         0,         far/(near - far),       -1,
               0,         0,         far*near/(near - far), 0
            );
            */
            return matr;
        }
        public static Matrix3D PerspectiveFovRH(double fieldOfViewX, double aspectRatio, double zNear, double zFar)
        {
            double width = Math.Tan(fieldOfViewX/2),
                height = width / aspectRatio,
                depth = zNear - zFar;
            Matrix3D matr = new Matrix3D(
               width, 0, 0, 0,
               0, height, 0, 0,
               0, 0, zFar/depth, -1,
               0, 0, zNear*zFar/depth, 0
            );
            return matr;
        }
        public static Matrix3D LookAtRH(Point3D cameraPosition, Point3D cameraTarget, Vector3D cameraUpVector)
        {
            Vector3D zAxis = (cameraPosition - cameraTarget);
            zAxis.Normalize();
            Vector3D xAxis = Vector3D.CrossProduct(cameraUpVector, zAxis);
            xAxis.Normalize();
            Vector3D vecCameraPosition = new Vector3D(cameraPosition.X, cameraPosition.Y, cameraPosition.Z);
            Vector3D yAxis = Vector3D.CrossProduct(zAxis, xAxis);
            Matrix3D matr = new Matrix3D(
                xAxis.X, yAxis.X, zAxis.X, 0,
                xAxis.Y, yAxis.Y, zAxis.Y, 0,
                xAxis.Z, yAxis.Z, zAxis.Z, 0,
                -Vector3D.DotProduct(xAxis, vecCameraPosition), -Vector3D.DotProduct(yAxis, vecCameraPosition), -Vector3D.DotProduct(zAxis, vecCameraPosition), 1
            );
            return matr;
        }
        public static Matrix3D LookAtRH(Point3D cameraPosition, Vector3D lookDirection, Vector3D upDirection)
        {
            Vector3D zAxis = -lookDirection; 
            zAxis.Normalize();
            Vector3D xAxis = Vector3D.CrossProduct(upDirection, zAxis);
            xAxis.Normalize();
            Vector3D vecCameraPosition = new Vector3D(cameraPosition.X, cameraPosition.Y, cameraPosition.Z);
            Vector3D yAxis = Vector3D.CrossProduct(zAxis, xAxis);
            Matrix3D matr = new Matrix3D(
                xAxis.X, yAxis.X, zAxis.X, 0,
                xAxis.Y, yAxis.Y, zAxis.Y, 0,
                xAxis.Z, yAxis.Z, zAxis.Z, 0,
                -Vector3D.DotProduct(xAxis, vecCameraPosition), -Vector3D.DotProduct(yAxis, vecCameraPosition), -Vector3D.DotProduct(zAxis, vecCameraPosition), 1
            );
            return matr;
        }        
    }
}
