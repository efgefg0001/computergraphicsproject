﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders; 

namespace PhotonMappingApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Scene scene; 
        private IRender render;
        private int width;
        private int height;
        public MainWindow()
        {
            InitializeComponent();
            width = (int)sceletonViewPortPath.Width; height = (int)sceletonViewPortPath.Height;
            scene = SceneCreator.CreateScene();
            render = new SkeletonRender(sceletonViewPortPath);
            widthTxtBx.Text = "500";
            heightTxtBx.Text = "500";
            numOfPhotonsTxtBx.Text = "10000";
            numOfBouncesTxtBx.Text = "2";
            modelsLstBx.ItemsSource = scene.Models;
            lightSrcLstBx.ItemsSource = scene.LightSources;
            radiusTxtBx.Text = "0";
        }

        private void photonMappingButton_Click(object sender, RoutedEventArgs e)
        {
            int widthPh = int.Parse(widthTxtBx.Text), heightPh = int.Parse(heightTxtBx.Text);
            int numOfPh = int.Parse(numOfPhotonsTxtBx.Text);
            int numOfBounces = int.Parse(numOfBouncesTxtBx.Text);
            var radius = double.Parse(radiusTxtBx.Text);
            PhotonMappingViewport viewportWindow = new PhotonMappingViewport(scene, new Size(widthPh, heightPh), numOfPh, numOfBounces, radius);
            viewportWindow.Owner = this;
            viewportWindow.ShowDialog();
        }

        private void rotateForwardButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Rotate(new Quaternion(new Vector3D(1, 0, 0), -90));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height);
        }

        private void rotateBackwardButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Rotate(new Quaternion(new Vector3D(1, 0, 0), 90));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void rotateRightButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Rotate(new Quaternion(new Vector3D(0, 1, 0), 90));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void rotateLeftButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Rotate(new Quaternion(new Vector3D(0, 1, 0), -90));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateForwardButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(0, 0, -0.5));
            scene.Camera.TransformMatrix = Matrix3D.Multiply(scene.Camera.TransformMatrix, matr);
            render.RenderScene(scene, width, height); 
        }

        private void scalePlusButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Scale(new Vector3D(1.5, 1.5, 1.5));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateBackwardButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(0, 0, 0.5));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateRightButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(0.5, 0, 0));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateLeftButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(-0.5, 0, 0));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateUpButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(0, -0.5, 0));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void translateDownButton_Click(object sender, RoutedEventArgs e)
        {
            Matrix3D matr = Matrix3D.Identity; matr.Translate(new Vector3D(0, 0.5, 0));
            scene.Camera.TransformMatrix = matr;
            render.RenderScene(scene, width, height); 
        }

        private void Window_Loaded(object sender, EventArgs e)
        {
            currentCameraInfLbl.Content = scene.Camera.ToString();
            cameraPostionTxtBx.Text = scene.Camera.Position.ToString();
            targetPointTxtBx.Text = Point3D.Add(scene.Camera.Position, scene.Camera.LookDirection).ToString();
            cameraUpDirTxtBx.Text = scene.Camera.UpDirection.ToString();
            render.RenderScene(scene, width, height);

        }

        private void changeCameraSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            Point3D position = Point3D.Parse(cameraPostionTxtBx.Text);
            Point3D targetPoint = Point3D.Parse(targetPointTxtBx.Text);
            Vector3D upDir = Vector3D.Parse(cameraUpDirTxtBx.Text);
            scene.Camera.Position = position;
            scene.Camera.LookDirection = Point3D.Subtract(targetPoint, position);
            scene.Camera.UpDirection = upDir;
            currentCameraInfLbl.Content = scene.Camera.ToString();
            render.RenderScene(scene, width, height);
        }

        private void addLightSourceBtn_Click(object sender, RoutedEventArgs e)
        {
            var numOfLightSources = scene.LightSources.Count;
            var addOfLSWndw = new AdditionOfLightSource(scene);
            addOfLSWndw.Owner = this;
            addOfLSWndw.ShowDialog();
            if (numOfLightSources < scene.LightSources.Count)
            {
                render.RenderScene(scene, width, height);
                lightSrcLstBx.Items.Refresh();
            }
        }

        private void removeLightSourceBtn_Click(object sender, RoutedEventArgs e)
        {
            if (lightSrcLstBx.SelectedIndex != -1)
            {
                scene.LightSources.RemoveAt(lightSrcLstBx.SelectedIndex);
                lightSrcLstBx.Items.Refresh();
                render.RenderScene(scene, width, height);
            }
        }

        private void addModelBtn_Click(object sender, RoutedEventArgs e)
        {
            var numOfModels = scene.Models.Count;
            var addModelWndw = new AdditionOfModel(scene);
            addModelWndw.Owner = this;
            addModelWndw.ShowDialog();
            if(numOfModels < scene.Models.Count)
            {
                render.RenderScene(scene, width, height);
                modelsLstBx.Items.Refresh();
            }
        }

        private void removeModelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.modelsLstBx.SelectedIndex != -1)
            {
                scene.Models.RemoveAt(modelsLstBx.SelectedIndex);
                modelsLstBx.Items.Refresh();
                render.RenderScene(scene, width, height);
            }            
        }
    }
}
