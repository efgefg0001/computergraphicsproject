﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public interface LightSource
    {
        Point3D RandomPoint { get; }
        Point3D Position { get; set; }
        Power PowerOfSource { get; }
        Photon EmitPhoton();
        LightSource Copy();
        //string Description { get; set; }
    }
}
