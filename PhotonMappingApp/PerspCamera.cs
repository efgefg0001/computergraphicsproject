﻿using System;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public class PerspCamera : ProjCamera
    {
        private bool compView= true, compProj = true;
        private Matrix3D viewMatr = Matrix3D.Identity;
        private Matrix3D projMatr = Matrix3D.Identity;
        private Point3D position;
        private Vector3D lookDirection;
        private Vector3D upDirection;
        private double fieldOfViewX;
        private double aspectRatio;
        private double zNear;
        private double zFar;

        public PerspCamera()
        {
            ZNear = -1;
            ZFar = -100;
            FieldOfViewX = Math.PI;
            Position = new Point3D();
            LookDirection = new Vector3D(0, 0, 1);
            UpDirection = new Vector3D(0, 1, 0);
            AspectRatio = 1;
        }
        public Point3D Position 
        { 
            get { return position; } 
            set { position = value; compView = true; } 
        }
        public Vector3D LookDirection 
        {
            get { return lookDirection; }
            set { lookDirection = value; compView = true; } 
        }
        public Vector3D UpDirection 
        {
            get { return upDirection; }
            set { upDirection = value; compView = true; } 
        }
        public double FieldOfViewX 
        {
            get { return fieldOfViewX; }
            set { fieldOfViewX = value; compProj = true; } 
        }
        public double AspectRatio 
        {
            get { return aspectRatio; }
            set { aspectRatio = value; compProj = true; } 
        }
        public double ZNear 
        {
            get { return zNear; }
            set { zNear = value; compProj = true; } 
        }
        public double ZFar 
        {
            get { return zFar; }
            set { zFar = value; compProj = true; } 
        }

        public override Matrix3D ViewMatr
        {
            get
            {
                if (compView)
                {
                    viewMatr = ProjCamera.LookAtRH(Position, LookDirection, UpDirection);
                    compView = false;
                }
                return viewMatr; 
            }
        }
        public override Matrix3D ProjMatr
        {
            get
            {
                if (compProj)
                {
                    projMatr = ProjCamera.PerspectiveFovRH(FieldOfViewX, AspectRatio, ZNear, ZFar);
                    compProj = false;
                }
                return projMatr; 
            }
        }
        private Matrix3D transformMatrix = Matrix3D.Identity;
        public override Matrix3D TransformMatrix
        {
            get
            {
                return transformMatrix; 
            }
            set
            {
                transformMatrix = value;
                Position = Point3D.Multiply(Position, transformMatrix);
                LookDirection = Vector3D.Multiply(LookDirection, transformMatrix);
                UpDirection = Vector3D.Multiply(UpDirection, transformMatrix);
                transformMatrix = Matrix3D.Identity;
            }
        }
        public override string ToString()
        {
            return "Position = " + Position + "\nTarget point = " + Point3D.Add(Position, LookDirection) + "\nUp direction = " + UpDirection; 
        }
    }
}
