﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    public class Scene
    {
//        private PerspectiveCamera camera;

        public Scene()
        {
            Models = new List<Model>();
            LightSources = new List<LightSource>();
            Camera = new PerspCamera();  
        }
        public IList<Model> Models { get; set; }
        public IList<LightSource> LightSources { get; set; }
        public PerspCamera Camera { get; set; }
/*
        public SurfaceElement Intersect(Ray rayInCameraSpace)
        {

        }
*/
    }
}
