﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    /// <summary>
    /// Interaction logic for AdditionOfModel.xaml
    /// </summary>
    public partial class AdditionOfModel : Window
    {
        private Scene scene;
        public AdditionOfModel(Scene scene)
        {
            InitializeComponent();
            this.scene = scene;
        }
        private void CheckDiffAndSpec(Vector3D diff, Vector3D spec)
        {
            var sum = diff + spec;
            if (sum.X > 1 || sum.Y > 1 || sum.Z > 1)
                throw new Exception("Sum of diffuse reflectance and specular reflectance must be less or equal 1.");
        }
        private void CheckDiffAndSpec(float diff, float spec)
        {
            var sum = diff + spec;
            if (sum > 1)
                throw new Exception("Sum of diffuse reflectance and specular reflectance must be less or equal 1.");
        }
        private void endAddition_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var description = descriptionTxtBx.Text;
                var pos = Vector3D.Parse(positionTxtBx.Text);
                var width = double.Parse(widthTxtBx.Text);
                var height = double.Parse(heightTxtBx.Text);
                var deptn = double.Parse(depthTxtBx.Text);
                var diffRef = float.Parse(diffRefTxtBx.Text);//Vector3D.Parse(diffRefTxtBx.Text);
                var specRef = float.Parse(specRefTxtBx.Text);//Vector3D.Parse(specRefTxtBx.Text);
                CheckDiffAndSpec(diffRef, specRef);
                var colorDiff = colorPicker.SelectedColor;
                var colorSpec = specColorPicker.SelectedColor; 
                var glossyExponent = glossyExpTxtBx.Text.Trim().ToLower() == "inf" ? float.PositiveInfinity : float.Parse(glossyExpTxtBx.Text);
                var box = SceneCreator.CreateCube(new MaterialOfModel(diffRef, specRef, colorDiff, colorSpec, glossyExponent));
                box.TransformMatrix *= new ScaleTransform3D(new Vector3D(width/2, height/2, deptn/2)).Value;
                box.TransformMatrix *= new TranslateTransform3D(pos).Value;
                box.Description = description;
                scene.Models.Add(box);
                this.Close();
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }
    }
}
