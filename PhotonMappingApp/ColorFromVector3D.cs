﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public static class ColorFromVector3D
    {
        public static void LoadFromVector3D(this Color color, Vector3D vector)
        {
            color.ScR = (float)vector.X;
            color.ScG = (float)vector.Y;
            color.ScB = (float)vector.Z;
        }
    }
}
