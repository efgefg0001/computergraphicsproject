﻿using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public class Sphere
    {
        public Sphere()
        {
            Center = new Point3D();
            SurfacePoint = new Point3D();
        }
        public Point3D Center { get; set; }
        public double RadiusSquared 
        { 
            get
            {
                return Point3D.Subtract(SurfacePoint, Center).LengthSquared;
            }
        }
        public Point3D SurfacePoint { get; set; }
    }
}
