﻿using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    static class MirrorReflectanceVector
    {
        public static Vector3D ReflectAbout(this Vector3D inputW, Vector3D n)
        {
            var factor = 2 * Vector3D.DotProduct(n, inputW);
            var outW = Vector3D.Subtract(Vector3D.Multiply(n, factor), inputW);
            return outW;
        }
    }
}
