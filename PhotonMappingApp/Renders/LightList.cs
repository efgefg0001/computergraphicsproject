﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PhotonMappingApp.Renders
{
    class LightList
    {
        private Random rand = new Random();
        private LightSource[] lightsources;
        private float totalWattage;
        private double[] probabilities;
        public LightList(LightSource[] lightsources)
        {
            this.lightsources = lightsources;
            CompTotalWattAndProb(); 
        }
        private float ComputeSumOfBands(Color color)
        {
            float sum = color.ScR + color.ScG + color.ScB;
            return sum;
        }
        private double[] ComputeProbabilities()
        {
            double[] probabilities = new double[lightsources.Length];
            Color sumOfColor = Colors.Black;
            foreach(var source in lightsources)
                sumOfColor = Color.Add(sumOfColor, source.PowerOfSource.Color); 
            float sum = sumOfColor.ScR + sumOfColor.ScG + sumOfColor.ScB;
            for (var i = 0; i < probabilities.Length; ++i)
            {
                var sumWatInSource =  ComputeSumOfBands(lightsources[i].PowerOfSource.Color);
                probabilities[i] = sumWatInSource / sum; 
            }
            return probabilities;
        }
        private void CompTotalWattAndProb()
        {
            foreach (var source in lightsources)
                totalWattage += source.PowerOfSource.Wattage;
            probabilities = new double[lightsources.Length];
            for (var i = 0; i < probabilities.Length; ++i)
                probabilities[i] = lightsources[i].PowerOfSource.Wattage / totalWattage;
        }
        public int ChooseLightSource()
        {
            double urv = rand.NextDouble();
            double sum = 0, prev = sum;
            bool found = false;
            int res = 0; 
            for(var i = 0;i < probabilities.Length && !found; ++i)
            {
                sum += probabilities[i];
                if (urv < sum /*&& urv >= prev*/)
                {
                    found = true;
                    res = i;
                }
                //prev = sum;
            }
            return res;
        }
        public Photon[] EmitPhotons(int numOfPhotons)
        {
            Photon[] photons = new Photon[numOfPhotons];
            for(var i = 0; i < numOfPhotons; ++i)
            {
                var indexOfSource = ChooseLightSource();
                photons[i] = lightsources[indexOfSource].EmitPhoton();
                photons[i].Power.Wattage = totalWattage / numOfPhotons / 10;/*/(float)probabilities[indexOfSource]*/;
            }
            return photons;
        }
    }
}
