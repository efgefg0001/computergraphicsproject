﻿using System;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

namespace PhotonMappingApp.Renders
{
    class RayTracer : IRender
    {
        private PerspCamera renderCamera;
        private int width, height;
        private double widthDouble, heightDouble;
        private const double dpiX = 96;
        private const double dpiY = 96;
        private Pipeline pipeline = new Pipeline();
        private int numOfTasks = 2 * Environment.ProcessorCount;

        private IList<IList<Triangle>> cameraSpTriangles;
        private bool[][] cullBackFace;
        private IList<Sphere> cameraSpSpheres;
        private LightSource[] cameraSpLights;

        public RayTracer()
        {}
        private Task RenderPart(int yBeg, int count, BitmapBuffer buffer)
        {
            return Task.Run(
                () =>
                {
                    int yEnd = yBeg + count;
                    for(var y = yBeg; y < yEnd ; ++y)
                        for(var x = 0; x < width; ++x)
                        {
                            Ray ray = ComputeEyeRay(new Point(x + 0.5, y + 0.5), renderCamera);
                            double distance = double.PositiveInfinity;
                            Color outgoingRadiance = new Color();
                            for (int i = 0, numOfSpheres = cameraSpSpheres.Count; i < numOfSpheres; ++i)
                            {
                                if (Intersections.Intersect(ray, cameraSpSpheres[i]))
                                    for (int j = 0, numOfTr = cameraSpTriangles[i].Count; j < numOfTr; ++j)
                                    {
                                        if (!cullBackFace[i][j] && SampleRayTriangle(new Point(x, y), ray, cameraSpTriangles[i][j], //modelView,
                                            ref outgoingRadiance, ref distance))
                                        {
                                            buffer[x, y] = outgoingRadiance;
                                        }
                                    }
                            }
                     }
                }
            );
        }
        public BitmapSource RenderScene(Scene scene, int width, int height) 
        {
            Task<BitmapSource> task = RenderSceneAsync(scene, width, height);
            return task.Result;
        }
        private void BackfaceCulling()
        {
                    for(int i = 0, numOfModels = cameraSpSpheres.Count; i < numOfModels; ++i)
                        {

                            var modelTriangles = cameraSpTriangles[i];
                            for (int j = 0; j < modelTriangles.Count; j++)
                            {
                                Point3D point = modelTriangles[j].A.Location;
                                Ray ray = new Ray(new Point3D(), new Vector3D(point.X, point.Y, point.Z));
                                var normal = modelTriangles[j].NormalToTriangle;
                                if (Vector3D.DotProduct(ray.Direction, normal) > 0)
                                    cullBackFace[i][j] = true;
                            }
                        }
        }
        private void TransfSceneToCameraSpace(Scene scene)
        {
            var sceneInCamSp = pipeline.CameraSpace(scene);
            cameraSpTriangles = sceneInCamSp.Item1;
            cameraSpSpheres = sceneInCamSp.Item2;
            cameraSpLights = sceneInCamSp.Item3;
        }
        private double dX, dY, xMin, yMax;
        private double dXdivWidth, dYdivHeight;
        private void ComputeInitDataForRays()
        {
            double aspect = renderCamera.AspectRatio; 
            dX = -2 * renderCamera.ZNear * Math.Tan(renderCamera.FieldOfViewX / 2) ;
            dY = dX * aspect;
            xMin = -dX / 2;
            yMax = dY / 2;
            dXdivWidth = dX / widthDouble ;
            dYdivHeight = dY / heightDouble ;
        }
        public void Preprocess(Scene scene, int widthInp, int heightInp)
        {
            width = widthInp; height = heightInp;
            widthDouble = (double)width; heightDouble = (double)height;
            scene.Camera.AspectRatio = heightDouble / widthDouble; //widthDouble / heightDouble;
            renderCamera = scene.Camera;
            TransfSceneToCameraSpace(scene);
            ComputeInitDataForRays();
            cullBackFace = Culling.BackFace(cameraSpTriangles);
        }
        public async Task<BitmapSource> RenderSceneAsync(Scene scene, int widthInp, int heightInp)
        {
            Preprocess(scene, widthInp, heightInp);
            BitmapBuffer buffer = new BitmapBuffer(width, height);
            Task[] tasks = new Task[numOfTasks];
            int count = height / numOfTasks, lastIndex = numOfTasks - 1;
            var start = DateTime.Now; 
            for(var i = 0; i < lastIndex; ++i)
                tasks[i] = RenderPart(i * count, count, buffer);
            tasks[lastIndex] = RenderPart(lastIndex * count, height - lastIndex * count, buffer);
            await Task.WhenAll(tasks);
            var end = DateTime.Now;
            TimeSpan duration = end - start;
            MessageBox.Show(duration.Minutes + " " + duration.Seconds + " " + duration.Milliseconds); 
            return buffer.CreateBitmapSource(dpiX, dpiY, null);
        }
        public Ray ComputeEyeRay(Point point, PerspCamera camera)
        {
            Point3D start = new Point3D(xMin + point.X * dXdivWidth, 
                yMax - point.Y * dYdivHeight, 
                renderCamera.ZNear); 
            var direction = new Vector3D(start.X, start.Y, start.Z);
            direction.Normalize();
            return new Ray(start, direction); 
        }
        public bool SampleRayTriangle(Point point, Ray ray, Triangle triangle, //Matrix3D modelView,
            ref Color radiance, ref double distance)
        {
            double[] weights = new double[3];
            double d = Intersections.Intersect(ray, triangle, ref weights);
            if (d >= distance)
                return false;
            distance = d;
            Point3D intersectionPoint = Point3D.Add(ray.Origin, Vector3D.Multiply(ray.Direction, d));
            Vector3D n = triangle.NormalInA * weights[0] +
                triangle.NormalInB * weights[1] +
                triangle.NormalInC * weights[2];
            n.Normalize();
            Vector3D wO = -ray.Direction;
            Shade(triangle, intersectionPoint, n, wO, ref radiance);
            return true;
        }
        public void Shade(Triangle triangle, Point3D point, Vector3D n, Vector3D wO, ref Color outL)
        {
            outL = new Color(); 
            foreach (var light in cameraSpLights)
            {
                Vector3D offset = Point3D.Subtract(light.Position, point);
                double distanceToLight = offset.Length;
                Vector3D w_i = Vector3D.Divide(offset, distanceToLight);
                if(Visible(point, w_i, distanceToLight))
                {
                    float num = (float)(4 * Math.PI * Math.Pow(distanceToLight, 2) /* * Vector3D.DotProduct(wO, n)*/);
                    Power power = light.PowerOfSource;
                    float scR = power.Red/ num, scG = power.Green/ num, scB = power.Blue/ num;
                    Color L_i = new Color{A = 255, ScR = scR, ScG = scG, ScB = scB};
                    Color colorBsdf = triangle.Bsdf.evaluateFiniteScatteringDensity(w_i, wO, n);
                    colorBsdf.A = 255;
                    Color color = L_i.Multiply(colorBsdf);
                    Color summand = /*Color.Multiply(*/L_i.Multiply(colorBsdf);//, (float)Math.Max(0.0, -Vector3D.DotProduct(w_i, n)));
                    outL = Color.Add(outL, summand); 
                }
            }
        }
        public bool Visible(Point3D point, Vector3D direction, double distance)
        {
            
            double rayBumpEpsilon = 1e-4;
            Ray shadowRay = new Ray(Point3D.Add(point, Vector3D.Multiply(rayBumpEpsilon, direction)), direction);
            distance -= rayBumpEpsilon;
            double[] ignore = new double[3];
            for (int i = 0, numOfModels = cameraSpSpheres.Count; i < numOfModels; ++i)
            {
                if (Intersections.Intersect(shadowRay, cameraSpSpheres[i]))
                    for (int j = 0, numOfTr = cameraSpTriangles[i].Count; j < numOfTr; ++j)
                    {
                        if (Intersections.Intersect(shadowRay, cameraSpTriangles[i][j], ref ignore) < distance)
                            return false;
                    }
            }
            return true;
        }
    }
}
