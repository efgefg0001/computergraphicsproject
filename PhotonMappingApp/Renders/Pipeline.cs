﻿using System;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Collections.Generic;

namespace PhotonMappingApp.Renders
{
    class Pipeline
    {
        public Pipeline()
        {
            ModelViewMatrix = Matrix3D.Identity;
            ProjMatrix = Matrix3D.Identity;
            MatrixForClip = Matrix3D.Identity;
            ViewportMatrix = Matrix3D.Identity;
        }
        private Point factor;
        private Point summand;
        public void Preprocess()
        {
            var minX = -Math.Abs(RenderCamera.ZNear) * Math.Tan(RenderCamera.FieldOfViewX/2);
            var maxY = Math.Abs(minX) / RenderCamera.AspectRatio;
            var dX = 2 * Math.Abs(minX);
            var dY = 2 * maxY;
            var factorX = ViewPortSize.Width / dX;
            var subtrahendX = minX * factorX;
            var factorY = ViewPortSize.Height / dY;
            var minuendY = factorY * maxY;
            factor = new Point(factorX, factorY);
            summand = new Point(subtrahendX, minuendY);
        }
        public PerspCamera RenderCamera { get; set; }
        public Matrix3D ModelViewMatrix { get; set; }
        public Matrix3D ProjMatrix { get; set; }
        public Matrix3D MatrixForClip { get; set; }
        public Matrix3D ViewportMatrix { get; set; }
        public Size ViewPortSize { get; set; }
        public Point FromCameraToViewPort(Point4D point)
        {
            var factorZ = /*Math.Abs(*/RenderCamera.ZNear/*)*/ / /*Math.Abs(*/point.Z/*)*/;
            var xPr = point.X * factorZ;
            var yPr = point.Y * factorZ; 
            return new Point(factor.X * xPr - summand.X, summand.Y - factor.Y * yPr);
        }
        public Point FromCameraToViewPort(Point3D point)
        {
            var factorZ = /*Math.Abs(*/RenderCamera.ZNear/*)*/ / /*Math.Abs(*/point.Z/*)*/;
            var xPr = point.X * factorZ;
            var yPr = point.Y * factorZ; 
            return new Point(factor.X * xPr - summand.X, summand.Y - factor.Y * yPr);
        }
        public Point ImageSpace(Point3D point)
        {
            Point4D point4D = new Point4D(point.X, point.Y, point.Z, 1);
            point4D = Point4D.Multiply(point4D, ModelViewMatrix);// 
            point4D = Point4D.Multiply(point4D, ProjMatrix);
            //point4D = Point4D.Multiply(point4D, MatrixForClip);

            double W = point4D.W;
            point4D.X /= W; point4D.Y /= W; point4D.Z /= W; point4D.W = 1; 

            point4D = Point4D.Multiply(point4D, ViewportMatrix);
            return /*FromCameraToViewPort(point4D);*/new Point(point4D.X, point4D.Y);
        }
        private Point ViewPortFromProjection(Point projPoint)
        {
            double width = ViewportMatrix.M11, height = ViewportMatrix.M12;
            double xVP = (projPoint.X + 1) * width / 2;
            double yVP = height - (projPoint.Y + 1) * height / 2;
            return new Point(xVP, yVP);
        }
        public Point3D CameraSpace(Matrix3D modelView, Point3D point)
        {
            return Point3D.Multiply(point, modelView);
        }
        public Vector3D CameraSpace(Matrix3D modelView, Point3D position, Vector3D vector)
        {
            Point3D endVec = position + vector;
            Point3D newBeg = Point3D.Multiply(position, modelView), newEnd = Point3D.Multiply(endVec, modelView);
            return newEnd - newBeg;//Vector3D.Multiply(vector, modelView);
        }
        public Vector3D CameraSpace(Matrix3D modelView, Vector3D vector)
        {
            var n = vector * modelView;
            //n.Normalize();
            return n; 
        }
        public Triangle CameraSpace(Matrix3D modelView, Triangle triangle)
        {
            Vertex[] vertices = new Vertex[] {
                new Vertex(CameraSpace(modelView, triangle.A.Location), normal: CameraSpace(modelView, triangle.NormalInA)),
                new Vertex(CameraSpace(modelView, triangle.B.Location), normal: CameraSpace(modelView, triangle.NormalInB)),
                new Vertex(CameraSpace(modelView, triangle.C.Location), normal: CameraSpace(modelView, triangle.NormalInC))
            };
            return new StandaloneTriangle(vertices);
        }
        public Sphere CameraSpace(Matrix3D viewMatr, Sphere sphere)
        {
            Sphere res = new Sphere { Center = CameraSpace(viewMatr, sphere.Center), SurfacePoint = CameraSpace(viewMatr, sphere.SurfacePoint)};
            return res;
        }
        public Tuple<IList<IList<Triangle>>, IList<Sphere>, LightSource[]> CameraSpace(Scene scene)
        {
            int countOfModels = scene.Models.Count;
            var cameraSpSpheres = new List<Sphere>();//[countOfModels];
            var cameraSpTriangles = new List<IList<Triangle>>();//new Tuple<Triangle, bool>[countOfModels][];
            for(var i = 0; i < countOfModels; ++i)
            {
                Model model = scene.Models[i];
                Matrix3D modelView = model.TransformMatrix * scene.Camera.ViewMatr; 
                cameraSpSpheres.Add(CameraSpace(scene.Camera.ViewMatr, model.BoundingVolume));
                cameraSpTriangles.Add(new List<Triangle>());//new Tuple<Triangle, bool>[model.ModelMesh.Triplets.Length];
                foreach (var triangle in model.ModelMesh)
                    cameraSpTriangles[i].Add(CameraSpace(modelView, triangle));
            }
            int countOfLights = scene.LightSources.Count;
            var cameraSpLights = new LightSource[countOfLights];
            for(var i = 0; i < countOfLights; ++i)
            {
                LightSource source = scene.LightSources[i];
                cameraSpLights[i] = source.Copy();
                cameraSpLights[i].Position = CameraSpace(scene.Camera.ViewMatr, source.Position);
            }
            return new Tuple<IList<IList<Triangle>>, IList<Sphere>, LightSource[]>(cameraSpTriangles, cameraSpSpheres, cameraSpLights);
        }
    }
}
