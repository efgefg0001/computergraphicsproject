﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace PhotonMappingApp.Renders
{
    public interface IRender
    {
        void Preprocess(Scene scene, int width, int height);
        BitmapSource RenderScene(Scene scene, int width, int height);
        Task<BitmapSource> RenderSceneAsync(Scene scene, int width, int height);
    }
}
