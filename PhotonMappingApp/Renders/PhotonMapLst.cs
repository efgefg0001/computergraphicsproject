﻿using System.Windows.Media;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp.Renders
{
    public class PhotonMapLst : IPhotonMap
    {
        private IList<Photon> listPhotons = new List<Photon>(); 
        void IPhotonMap.Store(Photon ph)
        {
            listPhotons.Add(ph);
        }
        Color IPhotonMap.EstimateIrradiance(SurfaceElement surfElem, double maxDist)
        {
            var irrad = Colors.Black;
            var maxDistSq = maxDist*maxDist;
            var phs = from photon in listPhotons
                      where (photon.Position - surfElem.Location).LengthSquared <= maxDistSq
                      select new {Red = photon.Power.Red, Green = photon.Power.Green, Blue = photon.Power.Blue};
            foreach(var ph in phs)
            {
                //MessageBox.Show(ph.Red + " " + ph.Green + " " + ph.Blue);
                irrad.ScR += ph.Red; 
                irrad.ScG += ph.Green; 
                irrad.ScB += ph.Blue;
            }
//            MessageBox.Show(irrad.ToString());            
//            irrad *= (float)(1.0 / Math.PI / maxDistSq); irrad.A = 255;
//            MessageBox.Show(irrad.ToString());
            return irrad;
        }
    }
}
