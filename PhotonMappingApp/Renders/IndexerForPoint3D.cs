﻿using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public static class IndexerForPoint3D
    {
        private static double GetUsingIndex(Point3D point, int index)
        {
            switch (index)
            {
                case 0:
                    return point.X;
                case 1:
                    return point.Y;
                case 2:
                    return point.Z;
                default:
                    return 0;
            }
        }
        private static void SetUsingIndex(Point3D point, int index, double value)
        {
           switch(index)
           {
               case 0:
                   point.X = value;
                   break;
               case 1:
                   point.Y = value;
                   break;
               case 2:
                   point.Z = value;
                   break;
               default:
                   break;
           }
        }
        /*
        public static double this[this Point3D point, int index]
        {
            get { return getUsingIndex(point, index); }
            set { setUsingIndex(point, index, value); } 
        }
        */
        public static double GetAt(this Point3D point, int index)
        {
            return GetUsingIndex(point, index);
        }
        public static void SetAt(this Point3D point, int index, double value)
        {
            SetUsingIndex(point, index, value);
        }
    }
}
