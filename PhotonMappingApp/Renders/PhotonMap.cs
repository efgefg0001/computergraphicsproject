﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace PhotonMappingApp.Renders
{
    public class PhotonMap
    {
        private bool mode = true;
        private IList<Photon> listPhotons = new List<Photon>();
        public PhotonMap(int maxCount)
        {
            MinPhotonsForEstimation = 8;
            MaxCount = maxCount;
            Count = 0;
            photons = new Photon[MaxCount + 1];
            bboxMin = Enumerable.Repeat(1e8, 3).ToArray();
            bboxMax = Enumerable.Repeat(-1e8, 3).ToArray();
            costheta = (from i in Enumerable.Range(0, sizeOfAngleTables)
                       select Math.Cos(i * Math.PI/sizeOfAngleTables)
                       ).ToArray();
            sintheta = (from i in Enumerable.Range(0, sizeOfAngleTables)
                       select Math.Sin(i * Math.PI / sizeOfAngleTables) 
                        ).ToArray();
            cosphi = (from i in Enumerable.Range(0, sizeOfAngleTables)
                       select Math.Cos(2 * i * Math.PI / sizeOfAngleTables)
                     ).ToArray();
            sinphi = (from i in Enumerable.Range(0, sizeOfAngleTables)
                      select Math.Sin(2 * i * Math.PI / sizeOfAngleTables)
                     ).ToArray();

        }
        public void Store(Photon photon)
        {
//            if (Count > MaxCount)
//                return;
//            Count += 1;
            if(Count < MaxCount)
            {
                if (mode) listPhotons.Add(photon);
                Count += 1;
                photons[Count] = photon;
                bBoxMin.X = Math.Min(bBoxMin.X, photon.Position.X); bBoxMax.X = Math.Max(bBoxMax.X, photon.Position.X);
                bBoxMin.Y = Math.Min(bBoxMin.Y, photon.Position.Y); bBoxMax.Y = Math.Max(bBoxMax.Y, photon.Position.Y);
                bBoxMin.Z = Math.Min(bBoxMin.Z, photon.Position.Z); bBoxMax.Z = Math.Max(bBoxMax.Z, photon.Position.Z);
            }

        }
        private Point3D bBoxMin = new Point3D(1e8, 1e8, 1e8);
        private Point3D bBoxMax = new Point3D(-1e8, -1e8, -1e8);
        public void ScalePhotonPower(float scale)
        {
            var lastIndex = prevScale + Count;
            for (var i = prevScale; i < lastIndex; ++i)
                photons[i].Power.Wattage *= scale;
            prevScale = lastIndex;
        }
        public int MinPhotonsForEstimation { get; set; } 
        private Color EstimateIrradiance(SurfaceElement surfElem, double maxDist)
        {
            var irrad = Colors.Black;
            var maxDistSq = maxDist*maxDist;
            if (Math.Abs(maxDistSq) > 1e-5)
            {
                var phs = from photon in listPhotons
                          where (photon.Position - surfElem.Location).LengthSquared <= maxDistSq
                          select new { Red = photon.Power.Red, Green = photon.Power.Green, Blue = photon.Power.Blue };
                foreach (var ph in phs)
                {
                    //MessageBox.Show(ph.Red + " " + ph.Green + " " + ph.Blue);
                    irrad.ScR += ph.Red;
                    irrad.ScG += ph.Green;
                    irrad.ScB += ph.Blue;
                }
                irrad *= (float)(1.0 / Math.PI / maxDistSq) / phs.Count() ; irrad.A = 255;
            }
            return irrad;
        }
        public Color EstimateIrradiance(SurfaceElement surfElem, double maxDist, int numOfPhot)
        {
            Color irrad = Colors.Black;
            if(mode)
                return EstimateIrradiance(surfElem, maxDist);
            var np = new NearestPhotons
            {
                Pos = surfElem.Location,
                Max = numOfPhot,
                Found = 0,
                GotHeap = 0,
                Dist2 = new double[numOfPhot + 1],
                Index = new Photon[numOfPhot + 1]
            };
            np.Dist2[0] = maxDist * maxDist;
            LocatePhotons(np, 1);
            //if (np.Found >= MinPhotonsForEstimation)
                for (var i = 1; i <= np.Found; ++i)
                {
                    var p = np.Index[i];
                    //if (Vector3D.DotProduct(p.Direction, surfElem.ShadingNormal)<0)
                    {
                        irrad.ScR += p.Power.Red; irrad.ScG += p.Power.Green; irrad.ScB += p.Power.Blue;
                    }
                }
                var tmp = (float)(1 / Math.PI / np.Dist2[0]);
                irrad.ScR *= tmp; irrad.ScG *= tmp; irrad.ScB *= tmp; irrad.A = 255;
            return irrad;
        }

        // ?
        public void LocatePhotons(NearestPhotons np, int index)
        {
            var p = photons[index];
            if (p == null)
                return;
            double dist1;
            if(index < halfCount)
            {
                dist1 = np.Pos.GetAt(p.Plane) - p.Position.GetAt(p.Plane);
                if (dist1 > 0)
                {
                    LocatePhotons(np, 2 * index + 1);
                    if (dist1 * dist1 < np.Dist2[0])
                        LocatePhotons(np, 2 * index);
                }
                else
                {
                    LocatePhotons(np, 2 * index);
                    if (dist1 * dist1 < np.Dist2[0])
                        LocatePhotons(np, 2 * index + 1);
                }
            }
            dist1 = p.Position.GetAt(0) - np.Pos.GetAt(0);
            var dist2 = dist1 * dist1;
            dist1 = p.Position.GetAt(0) - np.Pos.GetAt(1);
            dist2 += dist1 * dist1;
            dist1 = p.Position.GetAt(2) - np.Pos.GetAt(2);
            dist2 += dist1 * dist1;
            if (dist1 < np.Dist2[0])
            {
                if (np.Found < np.Max)
                {
                    np.Found += 1;
                    np.Dist2[np.Found] = dist2;
                    np.Index[np.Found] = p;
                }
                else
                {
                    int j, parent;
                    if (np.GotHeap == 0)
                    {
                        Photon phot;
                        double dst2;
                        var halfFound = np.Found >> 1;
                        for (var k = halfFound; k >= 1; --k)
                        {
                            parent = k;
                            phot = np.Index[k];
                            dst2 = np.Dist2[k];
                            while (parent <= halfFound)
                            {
                                j = parent + parent;
                                if (j < np.Found && np.Dist2[j] < np.Dist2[j + 1])
                                    j++;
                                if (dst2 >= np.Dist2[j])
                                    break;
                                np.Dist2[parent] = np.Dist2[j];
                                np.Index[parent] = np.Index[j];
                                parent = j;
                            }
                            np.Dist2[parent] = dst2;
                            np.Index[parent] = phot;
                        }
                        np.GotHeap = 1;
                    }
                    parent = 1;
                    j = 2;
                    while (j <= np.Found)
                    {
                        if (j < np.Found && np.Dist2[j] < np.Dist2[j + 1])
                            j++;
                        if (dist2 > np.Dist2[j])
                            break;
                        np.Dist2[parent] = np.Dist2[j];
                        np.Index[parent] = np.Index[j];
                        parent = j;
                        j += j;
                    }
                    if(dist2 < np.Dist2[parent])
                    {
                        np.Index[parent] = p;
                        np.Dist2[parent] = dist2;
                    }
                    np.Dist2[0] = np.Dist2[1];
                }
            }
        }
        private void BalanceSegment(Photon[] pbal, Photon[] porg, int ind,
            int start, int end)
        {

            if (ind > Count /*halfCount*/)
                return;
            var median = 1;
            var numOfElem = end - start + 1;
            while (4 * median <= end - start + 1/*numOfElem*/)
                median += median;
            if (3 * median <= end - start + 1/*numOfElem*/)
            {
                median += median;
                median += start - 1;
            }
            else
                median = end - start + 1;//numOfElem;
            var axis = 2;
            if (bBoxMax.X - bBoxMin.X > bBoxMax.Y - bBoxMin.Y &&
                bBoxMax.X - bBoxMin.X > bBoxMax.Z - bBoxMin.Z)
                axis = 0;
            else if (bBoxMax.Y - bBoxMin.Y > bBoxMax.Z - bBoxMin.Z)
                axis = 1;
            MedianSplit(porg, start, end, median, axis);
            if(ind >= pbal.Length)
                MessageBox.Show("index = " + ind + " pbal.Length = " + pbal.Length + " median = " + median + " start = " + start + " end = " + end);
            pbal[ind] = porg[median];
            pbal[ind].Plane = axis;
            if(median > start)
            {
                if (start < median - 1)
                {
                    var tmp = bBoxMax.GetAt(axis);
                    bBoxMax.SetAt(axis, pbal[ind].Position.GetAt(axis));
                    BalanceSegment(pbal, porg, 2 * ind, start, median - 1);
                    bBoxMax.SetAt(axis, tmp);
                }
                else
                {
                    if (2 * ind >= pbal.Length)
                        MessageBox.Show(2 * ind + " " + start); 
                    pbal[2 * ind] = porg[start];
                }
            }
            if (median < end)
                if (median + 1 < end)
                {
                    var tmp = bBoxMin.GetAt(axis);
                    bBoxMin.SetAt(axis, pbal[ind].Position.GetAt(axis));
                    BalanceSegment(pbal, porg, 2 * ind + 1, median + 1, end);
                    bBoxMin.SetAt(axis, tmp);
                }
                else
                    pbal[2 * ind + 1] = porg[end];
        }
        public void Swap(Photon[] ph, int a, int b)
        {
            var ph2 = ph[a];
            ph[a] = ph[b];
            ph[b] = ph2;
        }
        private void MedianSplit(Photon[] p, int start, int end, int median, int axis)
        {
            int left = start, right = end;
            while(right > left)
            {
                var v = p[right].Position.GetAt(axis);
                var i = left - 1;
                var j = right;
                while(true/*i < j*/)
                {
                    while (p[++i].Position.GetAt(axis) < v) ;
                    while (p[--j].Position.GetAt(axis) > v && j > left) ;
                    if (i >= j) break;
                    Swap(p, i, j); 
                }
                Swap(p, i, right);
                if (i >= median) right = i - 1;
                if (i <= median) left = i + 1;
            }
        }
        public void Balance()
        {
            if (mode) return;
            if(Count > 1)
            {
                var pa1 = new Photon[Count + 1];
                var pa2 = new Photon[Count + 1];
                for(var i = 0; i <= Count; ++i)
                    pa2[i] = photons[i];
//                pa2 = photons;
                BalanceSegment(pa1, pa2, 1, 1, Count);
                int d, j = 1, foo = 1;
                var fooPhoton = photons[j];
                for (var i = 1; i <= Count; ++i)
                {
                    d = Array.FindIndex(photons, ph => ph == pa1[j]);
                    pa1[j] = null;
                    if (d != foo)
                        photons[j] = photons[d];
                    else
                    {
                        photons[j] = fooPhoton;
                        if(i < Count)
                        {
                            for (; foo <= Count; ++foo)
                                if (pa1[foo] != null)
                                    break;
                            fooPhoton = photons[foo];
                            j = foo;
                        }
                        continue;
                    }
                    j = d;
                }
            }
            halfCount = Count / 2 - 1;
        }
        private int halfCount = 0;
        /*
        public Vector3D PhotonDir(Photon photon)
        {

        }
        */
        public int Count { get; private set; }
        public int MaxCount { get; private set; }
        private int prevScale = 1;
        private Photon[] photons;
        private double[] bboxMin;
        private double[] bboxMax;

        private const int sizeOfAngleTables = 256;
        private double[] costheta;
        private double[] sintheta;
        private double[] cosphi;
        private double[] sinphi;
    }
}
