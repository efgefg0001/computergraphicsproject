﻿using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class Ray
    {

        public Ray(Point3D origin, Vector3D dir)
        {
            Origin = origin;
            Direction = dir;
        }

        public Point3D Origin { get; private set; }
        public Vector3D Direction { get; private set; }
        public Ray BumpedRay(double coeff, Vector3D n)
        {
            var newOrigin = Point3D.Add(Origin, Vector3D.Multiply(coeff, n));
            return new Ray(newOrigin, Direction);
        }
    }
}
