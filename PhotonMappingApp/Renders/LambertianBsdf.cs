﻿using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class LambertianBsdf : IBsdf
    {
        private Color k_L;
        public LambertianBsdf(Color k_L)
        {
            this.k_L = k_L;
        }

        public Color evaluateFiniteScatteringDensity(Vector3D w_i, Vector3D w_o, Vector3D n)
        {
            float coef = (float)(1 / Math.PI);
            return Color.Multiply(k_L, coef); 
        }
    }
}
