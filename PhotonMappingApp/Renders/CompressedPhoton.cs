﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp.Renders
{
    class CompressedPhoton
    {
        public CompressedPhoton(Photon photon)
        {
            
        }
        public double[] Pos { get; private set; }
        public short Plane { get; set; }
        public ushort Theta { get; private set; }
        public ushort Phi { get; private set; }
        public double[] Power { get; private set; }
    }
}
