﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class SurfaceElement
    {
        private Random rand = new Random();
        public SurfaceElement(Point3D location, Vector3D geomNormal, Vector3D shadingNormal,
            MaterialOfModel material)
        {
            Location = location;
            GeomNormal = geomNormal;
            ShadingNormal = geomNormal; ShadingNormalIn = shadingNormal;
            Material = material;
        }
        public MaterialOfModel Material { get; private set; }
        public Point3D Location { get; private set; }
        public Vector3D GeomNormal { get; private set; }
        public Vector3D ShadingNormal { get; private set; }
        public Vector3D ShadingNormalIn { get; private set; }
        public ResultOfScattering Scatter(Vector3D inputW)
        {
            Vector3D n = ShadingNormal;
            var outW = new Vector3D();
            var coeff = Colors.Black; 
            double randNumber = rand.NextDouble();
            var diffColor =Material.DiffuseColor * Material.DiffuseReflectance;
//            float diffuseAvg = Material.Average(diffColor);
            randNumber -= Material.DiffuseReflectance;//diffuseAvg;
            if(randNumber <= 0)
            {
                coeff = diffColor; //* (1f/diffuseAvg);
                coeff.A = 255;
                outW = rand.NextCosHemispereRandom(n);
                return new ResultOfScattering(true, outW, coeff);
            } 
            var specColor = Material.GlossyColor * Material.SpecularReflectance;
//            var specularAvg = Material.Average(specColor);
            randNumber -= Material.SpecularReflectance;//specularAvg;
            if(randNumber <= 0)
            {
                coeff = specColor; //* (1f/specularAvg);
                coeff.A = 255;
                outW = inputW.ReflectAbout(n);
                return new ResultOfScattering(true, outW, coeff);
            }
            return new ResultOfScattering(false, outW, coeff);
        }
        public IList<Impulse> GetBsdfImpulses(Vector3D inputW, int numOfImp)
        {
            var impulses = new List<Impulse>();
            var n = ShadingNormal;
            for (var i = 0; i < numOfImp; ++i)
                impulses.Add(new Impulse
                {
                    Direction = inputW.ReflectAbout(n),
                    Magnitude = Material.GlossyColor * Material.SpecularReflectance
                });
            return impulses;
        }
        /*
        public Color EvaluateBsdf()
        {

        }
        */
    }
}
