﻿using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public static class CosHemishpereRandom
    {
        public static Vector3D NextCosHemispereRandom(this Random rand, Vector3D n)
        {
            Vector3D vec = new Vector3D();
//            do
            {
                var angle = 2 *Math.PI * rand.NextDouble();
                var s = rand.NextDouble();
                var r = Math.Sqrt(1 - s);
                vec.X = r * Math.Cos(angle); vec.Y = Math.Sqrt(s); vec.Z = r * Math.Sin(angle);
            }
//            while(Vector3D.DotProduct(vec, n) < 0);
            return vec;
        }
    }
}
