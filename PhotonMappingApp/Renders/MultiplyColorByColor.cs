﻿using System.Windows.Media;

namespace PhotonMappingApp.Renders
{
    public static class MultiplyColorByColor
    {
       public static Color Multiply(this Color color1, Color color2)
       {
           Color res = new Color { ScA = color1.ScA * color2.ScA, 
           ScB = color1.ScB * color2.ScB, ScG = color1.ScG * color2.ScG,
           ScR = color1.ScR * color2.ScR};
           return res;
       }
    }
}
