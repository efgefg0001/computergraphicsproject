﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotonMappingApp.Renders
{
    class SkeletonRender : IRender
    {
        private Path sceletonViewPortPath;
        private StreamGeometry geometry = new StreamGeometry();
        private Pipeline pipeline = new Pipeline();

        public SkeletonRender(Path sceletonViewPortPath)
        {
            this.sceletonViewPortPath = sceletonViewPortPath;
            this.sceletonViewPortPath.Data = geometry;
        }
        private void DrawTriangle(Triangle triangle, StreamGeometryContext streamGeomContext)
        {
            Point pointA = pipeline.FromCameraToViewPort(triangle.A.Location),
                  pointB = pipeline.FromCameraToViewPort(triangle.B.Location),
                  pointC = pipeline.FromCameraToViewPort(triangle.C.Location); 
            streamGeomContext.BeginFigure(pointA, true /* is filled */, true /* is closed */);
            streamGeomContext.LineTo(pointB, true /* is stroked */, false /* is smooth join */);
            streamGeomContext.LineTo(pointC, true /* is stroked */, false /* is smooth join */);
        }
        public void Preprocess(Scene scene, int width, int height)
        {
            pipeline.ViewportMatrix = Transforms.ViewPortMatrix(0, 0, width, height);
            pipeline.ViewPortSize = new Size((double)width, (double)height);
            scene.Camera.AspectRatio = (double)width / height;//(double)(height) / width; 
            PerspCamera renderCamera = scene.Camera;
            pipeline.ProjMatrix = renderCamera.ProjMatr;
            pipeline.RenderCamera = scene.Camera;
            pipeline.Preprocess();
            TransfSceneToCameraSpace(scene);
            cullBackFace = Culling.BackFace(cameraSpModels.Item1);
        }
        private void TransfSceneToCameraSpace(Scene scene)
        {
            var sceneInCamSp = pipeline.CameraSpace(scene);
            cameraSpModels = new Tuple<IList<IList<Triangle>>, IList<Sphere>>(sceneInCamSp.Item1, sceneInCamSp.Item2);
            cameraSpLights = sceneInCamSp.Item3;
            materials = new MaterialOfModel[scene.Models.Count];
            for (var i = 0; i < materials.Length; ++i)
                materials[i] = scene.Models[i].Material;
        }
        private bool[][] cullBackFace; 
        private Tuple<IList<IList<Triangle>>, IList<Sphere>> cameraSpModels;
        private LightSource[] cameraSpLights;
        private MaterialOfModel[] materials;
        public BitmapSource RenderScene(Scene scene, int width, int height)
        {
            Preprocess(scene, width, height);
            using (StreamGeometryContext streamGeomContext = geometry.Open())
            {
                for (var i = 0; i < cameraSpModels.Item1.Count; ++i)
                {
                    var model = cameraSpModels.Item1[i];
                    //pipeline.ModelViewMatrix = model.TransformMatrix * renderCamera.ViewMatr*renderCamera.TransformMatrix;
                    for (var j = 0; j < model.Count; ++j)
                    {
                        if (!cullBackFace[i][j])
                            DrawTriangle(model[j], streamGeomContext);
                    }
                }
                foreach (var lighSource in cameraSpLights)
                    DrawLightSource(lighSource, streamGeomContext);
            }
            return null; 
        }
        private void DrawLightSource(LightSource source, StreamGeometryContext context)
        {
            var imgSpPos = pipeline.FromCameraToViewPort(source.Position);
            context.DrawGeometry(new EllipseGeometry(imgSpPos, 10, 10));
        }

        public async Task<BitmapSource> RenderSceneAsync(Scene scene, int width, int height)
        {
            return RenderScene(scene, width, height);
        }
    }
}
