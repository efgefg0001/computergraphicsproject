﻿using System.Windows.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp.Renders
{
    public interface IPhotonMap
    {
        void Store(Photon ph);
        Color EstimateIrradiance(SurfaceElement surfElem, double maxDist);
    }
}
