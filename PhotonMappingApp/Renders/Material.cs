﻿using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class MaterialOfModel
    {
        public MaterialOfModel(float diffRefl, float specRefl, Color diffColor, Color glossyColor, float glossyExponent)
        {
            DiffuseReflectance = diffRefl;//new Color{A=255, ScR = diffRefl, ScG = diffRefl, ScB = diffRefl};
            DiffuseColor = diffColor;
            SpecularReflectance = specRefl;
            GlossyExponent = glossyExponent;
            GlossyColor = glossyColor;
            Bsdf = new BlinnPhongBsdf(diffColor * diffRefl, glossyColor * specRefl, glossyExponent);
        }
        public float DiffuseReflectance { get; private set; }
        public Color DiffuseColor { get; private set; }

        public float SpecularReflectance { get; private set; }
        public Color GlossyColor { get; private set; }
        public float GlossyExponent { get; private set; }
        public float Average(Color reflectance)
        {
            return (reflectance.ScR + reflectance.ScG + reflectance.ScB) / 3;
        }
        public IBsdf Bsdf { get; private set; }
        public Color Color { get; private set; }
    }
}
