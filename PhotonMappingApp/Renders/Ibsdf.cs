﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public interface IBsdf
    {
        Color evaluateFiniteScatteringDensity(Vector3D w_i, Vector3D w_o, Vector3D n);
    }
}
