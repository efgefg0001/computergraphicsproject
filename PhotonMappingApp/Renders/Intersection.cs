﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp.Renders
{
    public class Intersection<T>
    {
        public Intersection(bool intersects, T data) 
        {
            Intersects = intersects;
            Data = data;
        }
        public bool Intersects { get; private set; }
        public T Data { get; private set; }
    }
}
