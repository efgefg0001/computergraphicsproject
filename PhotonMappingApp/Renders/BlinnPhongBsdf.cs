﻿using System.Windows.Media;
using System.Windows.Media.Media3D;
using System;

namespace PhotonMappingApp.Renders
{
    class BlinnPhongBsdf : IBsdf
    {
        private Color k_L;
        private Color k_G;
        private float s;
        public BlinnPhongBsdf(Color k_L, Color k_G, float s)
        {
            this.k_L = k_L;
            k_L.A = 255;
            this.k_G = k_G;
            k_G.A = 255;
            this.s = s;
        }
        public Color evaluateFiniteScatteringDensity(Vector3D w_i, Vector3D w_o, Vector3D n)
        {
            Vector3D w_h = w_i + w_o;
            w_h.Normalize();
            Color summand = Colors.Black;
            if (s != float.PositiveInfinity)
            {
                float number = (float)(Math.Pow(Math.Max(0.0, Math.Abs(Vector3D.DotProduct(w_h, n))), s) / 8.0);
                summand = Color.Multiply(k_G, (s + 8.0f) * number);
                summand.A = 255;
            }
            var res = k_L + summand; res.A = 255;
            return res; 
        }
    }
}
