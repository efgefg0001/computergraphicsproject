﻿using System;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class ResultOfScattering
    {
        public ResultOfScattering(bool isScattered, Vector3D outW, Color outWeight)
        {
            IsScattered = isScattered;
            OutW = outW;
            OutWeight = outWeight;
        }
        public bool IsScattered { get; private set; }
        public Vector3D OutW { get; private set; }
        public Color OutWeight { get; private set; }
    }
}
