﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    class Intersections
    {
        public static bool Intersect(Ray ray, Sphere sphere)
        {
            bool res = false;
            Vector3D eo = Point3D.Subtract(sphere.Center, ray.Origin); //Vector.Minus(Center, ray.Start);
            double v = Vector3D.DotProduct(eo, ray.Direction); //(eo, ray.Dir);
            double dist;
            if(v >= 0)
            {
                double disc = sphere.RadiusSquared - (Vector3D.DotProduct(eo, eo) - Math.Pow(v, 2));
                res = disc < 0 ? false : true;
            }
            return res;
        }
        private static SurfaceElement ComputeSurfaceElement(Tuple<double, Triangle, double[], MaterialOfModel> minIntersection, Ray ray)
        {
            var distance = minIntersection.Item1;
            var triangle = minIntersection.Item2;
            var weights = minIntersection.Item3;
            var material = minIntersection.Item4;
            Point3D intersectionPoint = Point3D.Add(ray.Origin, Vector3D.Multiply(ray.Direction, distance));
            Vector3D shadingNormal = triangle.NormalInA * weights[0] +
            triangle.NormalInB * weights[1] +
            triangle.NormalInC * weights[2];
            shadingNormal.Normalize();
            //var shadingNormal = triangle.NormalToTriangle;
            return new SurfaceElement(intersectionPoint, triangle.NormalToTriangle, shadingNormal, material);
        }
        public static Intersection<SurfaceElement> Intersect(Ray ray, Tuple<IList<IList<Triangle>>, IList<Sphere>> sceneGeometry, bool[][] cullBackFace, IList<MaterialOfModel> materialsOfModels = null) 
        {
            var spheres = sceneGeometry.Item2;
            var models = sceneGeometry.Item1;
            bool intersects = false;
            double minDistance = double.PositiveInfinity;
            double[] minWeights = null;
            Triangle minTriangle = null;
            MaterialOfModel materialOfMinIntersection = null;
            for(var i = 0; i<spheres.Count;++i)
                if(Intersect(ray, spheres[i]))
                {
                    var j = 0;
                    foreach(var triangle in models[i])
                        //if(!cullBackFace[i][j++])
                        {
                            var weights = new double[3];
                            double distance = Intersect(ray, triangle, ref weights);
                            if (distance < minDistance && distance != double.PositiveInfinity)
                            {
                                minDistance = distance;
                                minWeights = weights;
                                minTriangle = triangle;
                                materialOfMinIntersection = materialsOfModels == null ? null: materialsOfModels[i];
                                intersects = true;
                            }
                    }
                }
            var surfEl = intersects ?
                ComputeSurfaceElement(new Tuple<double, Triangle, double[], MaterialOfModel>(minDistance, minTriangle, minWeights, materialOfMinIntersection), ray) :
                null;
            return new Intersection<SurfaceElement>(intersects, surfEl);
        }

        public static double Intersect(Ray ray, Triangle triangle, ref double[] weights)
        {

            var e1 = triangle.B.Location - triangle.A.Location; 
            var e2 = triangle.C.Location - triangle.A.Location; 
            var q = Vector3D.CrossProduct(ray.Direction, e2); 
            var a = Vector3D.DotProduct(e1, q);
            var s = ray.Origin - triangle.A.Location; 
            var r = Vector3D.CrossProduct(s, e1); 
            double dist = Vector3D.DotProduct(e2, r) / a;
            weights[1] = (Vector3D.DotProduct(s, q) / a);
            weights[2] = (Vector3D.DotProduct(ray.Direction, r) / a);
            weights[0] = (1 - (weights[1] + weights[2]));

            double epsilon = 1e-7;
            double epsilon2 = 1e-10;
            if (a <= epsilon || weights[0] < -epsilon2 || 
                weights[1] < -epsilon2 || weights[2] < -epsilon2
                || dist <= 0)
                return double.PositiveInfinity;
            else
                return dist;
        }
    }
}
