﻿using System.Collections.Generic;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class NearestPhotons
    {
        public NearestPhotons()
        {
//            Dist2 = new List<double>();
//            Index = new List<Photon>();
        }
        public int Max { get; set; }
        public int Found { get; set; }
        public int GotHeap { get; set; }
        public Point3D Pos { get; set; }
//        public IList<double> Dist2 { get; set; }
//        public IList<Photon> Index { get; set; }
        public double[] Dist2 { get; set; }
        public Photon[] Index { get; set; }
    }
}
