﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    public class Impulse
    {
        public Vector3D Direction { get; set; }
        public Color Magnitude { get; set; } 
    }
}
