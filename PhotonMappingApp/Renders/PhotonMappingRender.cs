﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders; 

namespace PhotonMappingApp
{
    class PhotonMappingRender : IRender
    {
        private Random rand = new Random();
        private int width, height;
        private double widthDouble, heightDouble;
        private PerspCamera renderCamera;
        private const double dpiX = 96;
        private const double dpiY = 96;
        private Pipeline pipeline = new Pipeline();
        private int numOfTasks = 2 * Environment.ProcessorCount;
        private LightList lightList;

        private Tuple<IList<IList<Triangle>>, IList<Sphere>> cameraSpModels;
        private MaterialOfModel[] materials;
        private bool[][] cullBackFace;
        private LightSource[] cameraSpLights;
        private int numOfEmittedPhotons = 0;
        private int maxBounces = 0;
        private double radius;
        public PhotonMappingRender(int numOfEmittedPhotons, int maxBounces, int primaryRaysPerPixel, double radius)
        {
            this.numOfEmittedPhotons = numOfEmittedPhotons;
            this.maxBounces = maxBounces;
            this.primaryRaysPerPixel = primaryRaysPerPixel;
            this.includeDiff = true;
            this.includeImpulse = true;
            this.radius = radius;
        }
        public BitmapSource RenderScene(Scene scene, int inpWidth, int inpHeight)
        {
            var task = RenderSceneAsync(scene, inpWidth, inpHeight);
            return task.Result;
        }
        public IPhotonMap BuildPhotonMap()
        {
            Photon[] photons = lightList.EmitPhotons(numOfEmittedPhotons);
            IPhotonMap phtnMap = new PhotonMapLst(); 
            foreach (var photon in photons)
            {
                var ips = PhotonTrace(photon);
                foreach (var ph in ips)
                    phtnMap.Store(ph);
            }
            return phtnMap;
        }
        public IList<Photon> PhotonTrace(Photon photon)
        {
            IList<Photon> ips = new List<Photon>();
            PhotonTraceHelper(photon, ips, 0);
            return ips;
        }
        public void PhotonTraceHelper(Photon photon, IList<Photon> ips, int bounces)
        {
            if (bounces > maxBounces)
                return;
            Ray ray = new Ray(photon.Position, photon.Direction);
            var intersection = Intersections.Intersect(ray, cameraSpModels, cullBackFace, materials);
            if(intersection.Intersects)
            {
                var surfEl = intersection.Data;
                if (bounces > 0)
                    ips.Add(new Photon(surfEl.Location, -ray.Direction, photon.Power));
                var w_i = -ray.Direction;
                var resOfScattering = surfEl.Scatter(w_i);
                if(resOfScattering.IsScattered)
                {
                    Ray r = new Ray(surfEl.Location, resOfScattering.OutW);
                    r = r.BumpedRay(
                        0.0001 * Math.Sign(Vector3D.DotProduct(surfEl.GeomNormal, resOfScattering.OutW)),
                        surfEl.GeomNormal);
                    Color clrP = photon.Power.Color, clrOW = resOfScattering.OutWeight;

                    var power = new Power(
                        //photon.Power.Color.Multiply(resOfScattering.OutWeight),
                        new Color {A = 255, ScR = clrP.ScR * clrOW.ScR, ScG = clrP.ScG * clrOW.ScG, ScB = clrP.ScB * clrOW.ScB},
                        photon.Power.Wattage);
                    if (power.Color.ScR == float.NaN || power.Color.ScG == float.NaN || power.Color.ScB == float.NaN)
                        MessageBox.Show("NaN");
                    PhotonTraceHelper(new Photon(r.Origin, r.Direction, power), ips, bounces + 1);
                }
            }
        }
        private void TransfSceneToCameraSpace(Scene scene)
        {
            var sceneInCamSp = pipeline.CameraSpace(scene);
            cameraSpModels = new Tuple<IList<IList<Triangle>>, IList<Sphere>>(sceneInCamSp.Item1, sceneInCamSp.Item2);
            cameraSpLights = sceneInCamSp.Item3;
            materials = new MaterialOfModel[scene.Models.Count];
            for (var i = 0; i < materials.Length; ++i)
                materials[i] = scene.Models[i].Material;
        }
        private double dX, dY, xMin, yMax, dXdivWidth, dYdivHeight;
        private void ComputeInitDataForRays()
        {
            double aspect = renderCamera.AspectRatio; 
            dX = -2 * renderCamera.ZNear * Math.Tan(renderCamera.FieldOfViewX / 2) ;
            dY = dX * aspect;
            xMin = -dX / 2;
            yMax = dY / 2;
            dXdivWidth = dX / widthDouble ;
            dYdivHeight = dY / heightDouble ;
            //includeDiff = false; 
        }
        private Ray ComputeEyeRay(Point point)
        {
            Point3D start = new Point3D(xMin + point.X * dXdivWidth, 
                yMax - point.Y * dYdivHeight, 
                renderCamera.ZNear); 
            var direction = new Vector3D(start.X, start.Y, start.Z);
            direction.Normalize();
            return new Ray(start, direction); 
        }
        public void Preprocess(Scene scene, int widthInp, int heightInp)
        {
            width = widthInp; height = heightInp;
            widthDouble = (double)width; heightDouble = (double)height;
            scene.Camera.AspectRatio = heightDouble / widthDouble; //widthDouble / heightDouble;
            renderCamera = scene.Camera;
            TransfSceneToCameraSpace(scene);
            lightList = new LightList(cameraSpLights);
            cullBackFace = Culling.BackFace(cameraSpModels.Item1);
            ComputeInitDataForRays();
        }
        private int primaryRaysPerPixel;
        private bool emit = true;
        private Color EstimateTotalRadiance(Ray ray, int depth)
        {
            Color outRad = Colors.Black;
/*
            if (emit)
            {
                outRad = Color.Add(outRad, EstimateEmittedLight(ray));
                outRad.A = 255;
            }
*/
            outRad += EstimateTotalScatteredRadiance(ray, depth);
            return outRad;
        }
        private bool includeImpulse = true;
        private bool includeDiff = true;
        public Color EstimateTotalScatteredRadiance(Ray ray, int depth)
        {
            var intersection = Intersections.Intersect(ray, cameraSpModels, cullBackFace, materials);
            var outRad = Colors.Black;
            if(intersection.Intersects)
            {
                outRad += EstimateReflectedDirectLight(ray, intersection.Data, depth);
                if (intersection.Data.Material.GlossyExponent == float.PositiveInfinity && (includeImpulse || depth > 0))
                {
                    outRad += EstimateImpulseScatteredIndirectLight(ray, intersection.Data, depth + 1);
                }
                if (includeDiff || depth > 0)
                    try
                    {
                        outRad += EstimateDiffuselyReflectedIndirectLight(ray, intersection.Data);
                    }
                    catch
                    { }
            }
            return outRad;
        }
        public Color EstimateDiffuselyReflectedIndirectLight(Ray ray, SurfaceElement surfEl)
        {
            return photonMap.EstimateIrradiance(surfEl, radius);
        }
        public Color EstimateReflectedDirectLight(Ray ray, SurfaceElement surfEl, int depth)
        {
            Color outRad = Colors.Black;
            outRad += EstimateDirectLightFromPointLights(ray, surfEl, depth);
            return outRad;
        }
        public Color EstimateDirectLightFromPointLights(Ray ray, SurfaceElement surfEl, int depth)
        {
            var outRad = new Color(); 
            foreach (var light in cameraSpLights)
            {
                Vector3D w_i = Point3D.Subtract(light.Position, surfEl.Location);
                double distanceToLight = w_i.Length;
                w_i.Normalize();
                if (Visible(surfEl.Location, w_i, distanceToLight))
                {
                    float num = (float)(4 * Math.PI * Math.Pow(distanceToLight, 2) /* * Vector3D.DotProduct(wO, n)*/);
                    Power power = light.PowerOfSource;
                    float scR = power.Red / num, scG = power.Green / num, scB = power.Blue / num;
                    Color L_i = new Color { A = 255, ScR = scR, ScG = scG, ScB = scB };
                    Color colorBsdf = surfEl.Material.Bsdf.evaluateFiniteScatteringDensity(w_i, -ray.Direction, surfEl.ShadingNormalIn);
                    //colorBsdf.A = 255;
                    var summand = L_i.Multiply(colorBsdf) * (float)Math.Max(0.0, Vector3D.DotProduct(w_i, surfEl.ShadingNormal));
                    //summand.A = 255;
                    outRad += summand; 
                    //outRad.A = 255;
                }
            }
            return outRad;             
        }

        public bool Visible(Point3D point, Vector3D direction, double distance)
        {
            var cameraSpSpheres = this.cameraSpModels.Item2; var cameraSpTriangles = this.cameraSpModels.Item1;
            double rayBumpEpsilon = 1e-4;
            Ray shadowRay = new Ray(Point3D.Add(point, Vector3D.Multiply(rayBumpEpsilon, direction)), direction);
            distance -= rayBumpEpsilon;
            double[] ignore = new double[3];
            for (int i = 0, numOfModels = cameraSpSpheres.Count; i < numOfModels; ++i)
            {
                if (Intersections.Intersect(shadowRay, cameraSpSpheres[i]))
                    for (int j = 0, numOfTr = cameraSpTriangles[i].Count; j < numOfTr; ++j)
                    {
                        if (Intersections.Intersect(shadowRay, cameraSpTriangles[i][j], ref ignore) < distance)
                            return false;
                    }
            }
            return true;
        }
        public Color EstimateImpulseScatteredIndirectLight(Ray ray, SurfaceElement surfEl, int depth)
        {
            Color outRad = Colors.Black;
            if (depth > maxBounces)
                return outRad;

            var impulses = surfEl.GetBsdfImpulses(-ray.Direction, 1);
            foreach(var impulse in impulses)
            {
                var r = new Ray(surfEl.Location, impulse.Direction);
                var factor = Math.Sign(Vector3D.DotProduct(surfEl.GeomNormal, r.Direction));
                r = r.BumpedRay(0.0001 * factor, surfEl.GeomNormal);
                outRad += impulse.Magnitude.Multiply(EstimateTotalScatteredRadiance(r, depth + 1)); 
                //outRad.A = 255;
            }
            outRad *= 1.0f / impulses.Count;
            return outRad;
        }
/*
        private Color EstimateEmittedLight(Ray ray)
        {
            var intersection = Intersections.Intersect(ray, cameraSpModels, materials);
            if(intersection.Intersects)
            {
                
            }
        }
*/
        private Task<Color> ComputePartOfPixelColor(int x, int y, int beg, int count)
        {
            return Task<Color>.Run(
                () =>
                {
                    Color outRad = Colors.Black;
                    var end = beg + count;
                    for (var i = beg; i < end; ++i)
                    {
                        Ray ray = ComputeEyeRay(new Point(x + rand.NextDouble(), y + rand.NextDouble()));
                        outRad = Color.Add(outRad, EstimateTotalRadiance(ray, 0));
                    }
                    return outRad;
                }
            );
        }
        private Color RenderPixel(int x, int y)
        {
            var outRad = Colors.Black; 
            for (var i = 0; i < primaryRaysPerPixel; ++i)
            {
                Ray ray = ComputeEyeRay(new Point(x + rand.NextDouble(), y + rand.NextDouble()));
                outRad += EstimateTotalRadiance(ray, 0);
            }
            return Color.Multiply(outRad, 1f / primaryRaysPerPixel);
        }
        private Task RenderPart(int yBeg, int count, BitmapBuffer buffer)
        {
            return Task.Run(
                async () =>
                {
                    int yEnd = yBeg + count;
                    for (var y = yBeg; y < yEnd; ++y)
                        for (var x = 0; x < width; ++x)
                            buffer[x, y] = RenderPixel(x, y);
                }
            );
        }
        private IPhotonMap photonMap;
        public async Task<BitmapSource> RenderSceneAsync(Scene scene, int widthInp, int heightInp)
        {
            Preprocess(scene, widthInp, heightInp);
            BitmapBuffer buffer = new BitmapBuffer(width, height);
            photonMap = BuildPhotonMap();
            Task[] tasks = new Task[numOfTasks];
            int count = height / numOfTasks, lastIndex = numOfTasks - 1;
            var start = DateTime.Now; 
            for(var i = 0; i < lastIndex; ++i)
                tasks[i] = RenderPart(i * count, count, buffer);
            tasks[lastIndex] = RenderPart(lastIndex * count, height - lastIndex * count, buffer);
            await Task.WhenAll(tasks);
            var end = DateTime.Now;
            TimeSpan duration = end - start;
            MessageBox.Show(duration.Minutes + " " + duration.Seconds + " " + duration.Milliseconds); 
            return buffer.CreateBitmapSource(dpiX, dpiY, null);
        }
    }
}
