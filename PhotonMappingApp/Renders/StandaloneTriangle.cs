﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp.Renders
{
    class StandaloneTriangle : Triangle
    {
        private Vertex[] vertices = new Vertex[3];
        public StandaloneTriangle(Vertex[] vertices ) : base(null, 1, 2, 3)
        {
            this.vertices = vertices;
        }

        public override Vertex A
        {
            get
            {
                return vertices[0];
            }
        }
        public override Vertex B
        {
            get
            {
                return vertices[1];
            }
        }
        public override Vertex C
        {
            get
            {
                return vertices[2];
            }
        }
        public override Vector3D NormalInA
        {
            get
            {
                return vertices[0].ComputeNormal(null);
            }
        }
        public override Vector3D NormalInB
        {
            get
            {
                return vertices[1].ComputeNormal(null);
            }
        }
        public override Vector3D NormalInC
        {
            get
            {
                return vertices[1].ComputeNormal(null);
            }
        }
    }
}
