﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    /// <summary>
    /// Interaction logic for AdditionOfLightSource.xaml
    /// </summary>
    public partial class AdditionOfLightSource : Window
    {
        private Scene scene;
        public AdditionOfLightSource(Scene scene)
        {
            InitializeComponent();
            this.scene = scene;
        }

        private void endAddition_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                var position = Point3D.Parse(positionTxtBx.Text);
                var color = colorPicker.SelectedColor;
                var wattage = float.Parse(wattageTxtBx.Text);
                scene.LightSources.Add(new PointLightSource(new Power(color, wattage), position));
                this.Close();
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }
            
        }

    }
}
