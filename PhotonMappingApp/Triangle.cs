﻿using System;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    public class Triangle 
    {
        private Mesh mesh;
        private int indexA;
        private int indexB;
        private int indexC;
        public Triangle(Mesh mesh, int indexA, int indexB, int indexC) 
        {
            this.mesh = mesh;
            this.indexA = indexA;
            this.indexB = indexB;
            this.indexC = indexC;
        }
//        public Mesh Mesh { get { return mesh; } }

        public virtual Vertex A { get { return mesh.Vertices[indexA]; } }
        public virtual Vector3D NormalInA { get { return A.ComputeNormal(mesh); } }

        public virtual Vertex B { get { return mesh.Vertices[indexB]; } }
        public virtual Vector3D NormalInB { get { return B.ComputeNormal(mesh); } }

        public virtual Vertex C { get { return mesh.Vertices[indexC]; } }
        public virtual Vector3D NormalInC { get { return C.ComputeNormal(mesh); } }

        public Vector3D NormalToTriangle 
        { 
            get 
            {
                Point3D pA = A.Location, pB = B.Location, pC = C.Location;
                Vector3D normal = Vector3D.CrossProduct((pB - pA), (pC - pA));
                normal.Normalize();
                return normal; 
            } 
        }
    }
}
