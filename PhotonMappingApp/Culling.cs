﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    class Culling
    {
        public static bool[][] BackFace(IList<IList<Triangle>> sceneInCameraSpace)
        {
            bool[][] cullBackFace = new bool[sceneInCameraSpace.Count][];
            for(int i = 0, numOfModels = sceneInCameraSpace.Count; i < numOfModels; ++i)
            {
                var modelMesh = sceneInCameraSpace[i];
                cullBackFace[i] = new bool[modelMesh.Count];
                var j = 0;
                foreach (var triangle in modelMesh)
                {
                    Point3D point = triangle.A.Location;
                    Ray ray = new Ray(new Point3D(), new Vector3D(point.X, point.Y, point.Z));
                    var normal = triangle.NormalToTriangle;
                    if (Vector3D.DotProduct(ray.Direction, normal) > 0)
                        cullBackFace[i][j++] = true;
                    else
                        cullBackFace[i][j++] = false;
                }
            }
            return cullBackFace;
        } 
    }
}
