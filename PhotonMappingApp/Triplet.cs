﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp
{
    public class Triplet
    {
        public Triplet(int indexA, int indexB, int indexC)
        {
            IndexA = indexA;
            IndexB = indexB;
            IndexC = indexC;
        }
        public int IndexA { get; private set; }
        public int IndexB { get; private set; }
        public int IndexC { get; private set; }
    }
}
