﻿using System;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    public class Model
    {
        private Mesh mesh;
        private Sphere boundingVolume;
        private Point3D ComputeModelCenter()
        {
            Point3D modelCenter = new Point3D();
            int countVert = mesh.Vertices.Length;
            foreach (var vertex in mesh.Vertices)
            {
                Point3D point = Point3D.Multiply(vertex.Location, TransformMatrix);
                modelCenter.X += point.X; modelCenter.Y += point.Y; modelCenter.Z += point.Z; 
            }
            modelCenter.X /= countVert; modelCenter.Y /= countVert; modelCenter.Z /= countVert;
            return modelCenter;
        }
        private double GetMaxSquareRadius(Point3D modelCenter)
        {
            double max = 0;
            foreach(var vertex in mesh.Vertices)
            {
                double dist = Point3D.Subtract(Point3D.Multiply(vertex.Location, TransformMatrix), modelCenter).LengthSquared;
                if (dist > max)
                    max = dist;
            }
            return max;
        }
        private Sphere ComputeBoundingVolume()
        {
            Point3D modelCenter = ComputeModelCenter();
            double radSquared = GetMaxSquareRadius(modelCenter);
            Point3D surfacePoint = new Point3D(modelCenter.X, modelCenter.Y + Math.Sqrt(radSquared), modelCenter.Z);  
            return new Sphere { Center = modelCenter, SurfacePoint = surfacePoint };
        }
        public Model(Mesh mesh, MaterialOfModel material)
        {
            this.mesh = mesh;
            Material = material;
            TransformMatrix = Matrix3D.Identity;
            BoundingVolume = ComputeBoundingVolume();
        }
        public string Description { get; set; }
        private Matrix3D transformMatrix;
        public Matrix3D TransformMatrix 
        { 
            get
            {
                return transformMatrix;
            }
            set
            {
                transformMatrix = value;
                BoundingVolume = ComputeBoundingVolume();
            }
        }
        public Mesh ModelMesh { get { return mesh; } }
        public Sphere BoundingVolume { get; private set; }
        public MaterialOfModel Material { get; private set; }
    }
}
