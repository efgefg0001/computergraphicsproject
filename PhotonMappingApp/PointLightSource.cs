﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Media;

namespace PhotonMappingApp
{
    public class PointLightSource : LightSource
    {
        private Random rand = new Random();
        private Power power;
        private Point3D pos;
        private Vector3D spotAxis;
        private double spotHalfAngle;
        public PointLightSource(Power power, Point3D pos, 
            Vector3D spotAxis, double spotHalfAngle)
        {
            this.power = power;
            this.pos = pos;
            this.spotAxis = spotAxis;
            this.spotHalfAngle = spotHalfAngle; 
        }
        public PointLightSource(Power power, Point3D pos)
        {
            this.power = power;
            this.pos = pos;
        }
        public PointLightSource(PointLightSource source)
        {
            this.power = source.power;
            this.pos = source.pos;
            this.spotAxis = source.spotAxis;
            this.spotHalfAngle = source.spotHalfAngle;
        }

        public LightSource Copy()
        {
            return new PointLightSource(this);
        }
        public Point3D RandomPoint { get { return pos; } }
        public Point3D Position
        { 
            get { return pos; }
            set { pos = value; }
        }
        public Power PowerOfSource 
        { 
            get
            {
                return power;//new Power(Color.Multiply(power.Color, (float)(1 - Math.Cos(spotHalfAngle))/2f),power.Wattage); 
            }
        }
        private Vector3D RandomDirection()
        {
            return new Vector3D(rand.NextDouble(), rand.NextDouble(), rand.NextDouble());
        }
        public Photon EmitPhoton()
        {
            Vector3D w_o;
            do
            {
               w_o = RandomDirection(); 
            } 
            //while(Vector3D.DotProduct(spotAxis, w_o) < Math.Cos(spotHalfAngle));
            while(w_o.LengthSquared > 1);
            var copyOfPower = PowerOfSource.Copy();
            return new Photon(Position, w_o, copyOfPower);//new Photon{Power = power, Position = this.Position, Direction = w_o};
        }
    }
}
