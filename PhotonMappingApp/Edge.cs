﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotonMappingApp
{
    class Edge
    {
        public Edge()
        {
            VertexBegin = 0; VertexEnd = 0;
            FaceRight = 0; FaceLeft = 0;
        }
        public Edge(int vertexBegin, int vertexEnd, int faceRight, int faceLeft)
        {
            VertexBegin = vertexBegin;
            VertexEnd = vertexEnd;
            FaceRight = faceRight;
            FaceLeft = faceLeft;
        }
        public int VertexBegin { get; set; }
        public int VertexEnd { get; set; }
        public int FaceRight { get; set; }
        public int FaceLeft { get; set; }
    }
}
