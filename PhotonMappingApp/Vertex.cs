﻿using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Collections;
using System.Collections.Generic;

namespace PhotonMappingApp
{
    public class Vertex
    {
        private Vector3D shadingNormal;
        public Vertex(Point3D location, int[] triplets = null, Vector3D normal = new Vector3D())
        {
            Location = location;
            Triplets = triplets;
            shadingNormal = normal; 
        }

        public Point3D Location { get; private set; }
        public Vector3D ComputeNormal(Mesh mesh = null)
        {
            if (mesh == null)
                return shadingNormal;
            else
            {
                Vector3D normal = new Vector3D();
                foreach (var triplet in Triplets)
                    normal += mesh[triplet].NormalToTriangle;
                //normal.Normalize();
                return normal;
            }
        }
        public int[] Triplets { get; private set; }
    }
}
