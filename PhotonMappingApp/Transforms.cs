﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    public class Transforms
    {
        public static Matrix3D MatrixForClipping(double cX, double cY, double cW, double cH, double zMin, double zMax)
        {
            Matrix3D matr = new Matrix3D(
                2/cW, 0, 0, 0,
                0, 2/cH, 0, 0,
                0, 0, 1/(zMax - zMin), 0,
                -1-2*cX/cW, 1-2*cY/cH, -zMin/(zMax - zMin), 1
            );
            return matr;
        }
        public static Matrix3D DefaultMatrixForClipping()
        {
            return Transforms.MatrixForClipping(-1, 1, 2, 2, -1/*0*/, 1);
        }
        public static Matrix3D ViewPortMatrix(double dwX, double dwY, double dwWidth, double dwHeight)
        {
            /*
            Matrix3D matr = new Matrix3D(
                dwWidth, 0, 0, 0,
                0, -dwHeight, 0, 0,
                0, 0, 1, 0,
                dwX, dwHeight + dwY, 0, 1
            );
             */
            Matrix3D matr = new Matrix3D(
                dwWidth/2, 0, 0, 0,
                0, -dwHeight/2, 0, 0,
                0, 0, 1, 0,
                dwX + dwWidth/2, dwHeight/2 + dwY, 0, 1
            );
            /*Matrix3D matr = new Matrix3D(
                dwWidth, dwHeight, dwX, dwY,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0
            );*/
            return matr;
        }

    }
}
