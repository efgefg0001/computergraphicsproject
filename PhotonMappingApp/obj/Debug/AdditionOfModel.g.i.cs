﻿#pragma checksum "..\..\AdditionOfModel.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3BD1C7E59422F0DF87376D0775498FD0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;


namespace PhotonMappingApp {
    
    
    /// <summary>
    /// AdditionOfModel
    /// </summary>
    public partial class AdditionOfModel : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox descriptionTxtBx;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox positionTxtBx;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox widthTxtBx;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox heightTxtBx;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox depthTxtBx;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.ColorPicker colorPicker;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox diffRefTxtBx;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox specRefTxtBx;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.ColorPicker specColorPicker;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox glossyExpTxtBx;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\AdditionOfModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button endAddition;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PhotonMappingApp;component/additionofmodel.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AdditionOfModel.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.descriptionTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.positionTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.widthTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.heightTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.depthTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.colorPicker = ((Xceed.Wpf.Toolkit.ColorPicker)(target));
            return;
            case 7:
            this.diffRefTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.specRefTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.specColorPicker = ((Xceed.Wpf.Toolkit.ColorPicker)(target));
            return;
            case 10:
            this.glossyExpTxtBx = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.endAddition = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\AdditionOfModel.xaml"
            this.endAddition.Click += new System.Windows.RoutedEventHandler(this.endAddition_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

