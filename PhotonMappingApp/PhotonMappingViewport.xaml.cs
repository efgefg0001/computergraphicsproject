﻿using System.Windows;
using System.Windows.Media;
using System.Threading.Tasks;
using PhotonMappingApp.Renders;
using System.Windows.Media.Imaging;

namespace PhotonMappingApp
{
    /// <summary>
    /// Interaction logic for PhotonMappingViewport.xaml
    /// </summary>
    public partial class PhotonMappingViewport : Window
    {
        private IRender render;
        private Scene scene;
        private bool shouldCompute = true;

        public PhotonMappingViewport(Scene scene, Size size, int numOfEmittedPhotons, int numOfBouces, double radius)
        {
            InitializeComponent();
            this.scene = scene;
            viewportImage.Width = size.Width; viewportImage.Height = size.Height;
            render = new PhotonMappingRender(numOfEmittedPhotons, numOfBouces, 1, radius);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
 //           if (shouldCompute)
//            {
                BitmapSource source = await render.RenderSceneAsync(scene, (int)viewportImage.Width, (int)viewportImage.Height); 
                viewportImage.Source = source; 
//                shouldCompute = false;
//            }            
        }
    }
}
