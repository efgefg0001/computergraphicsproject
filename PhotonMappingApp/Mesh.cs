﻿using System;
using System.Collections;
using System.Collections.Generic;
using PhotonMappingApp.Renders;

namespace PhotonMappingApp
{
    public class Mesh : IEnumerable<Triangle>
    {
        public Mesh(Vertex[] vertices, Triplet[] triplets)
        {
            Vertices = vertices;
            Triplets = triplets;
        }
        public Vertex[] Vertices { get; private set; }
        public Triplet[] Triplets { get; private set; }
        public IEnumerator<Triangle> GetEnumerator()
        {
            foreach (var triplet in Triplets) 
            {
                yield return new Triangle(this, triplet.IndexA, triplet.IndexB, triplet.IndexC); 
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }       
        public Triangle GetTriangleFaceAt(int tripletIndex)
        {
            var triplet = Triplets[tripletIndex];
            return new Triangle(this, triplet.IndexA, triplet.IndexB, triplet.IndexC);
        }
        public Triangle this[int faceIndex]
        { 
            get { return GetTriangleFaceAt(faceIndex); } 
        }

    }
}
