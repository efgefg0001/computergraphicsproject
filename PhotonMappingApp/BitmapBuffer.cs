﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PhotonMappingApp
{
    class BitmapBuffer
    {
        private byte[] pixelData;
        private int bytesPerPixel;
        public BitmapBuffer(int width, int height)
        {
            Width = width; Height = height;
            PixelFormatOfBuffer = PixelFormats.Bgr24;
//            PixelFormatOfBuffer = PixelFormats.Bgra32;
            bytesPerPixel = PixelFormatOfBuffer.BitsPerPixel / 8;
            Stride = Width * bytesPerPixel;
            pixelData = new byte[Stride * Height];
        }
        public int Width { get; private set; }
        public int Stride { get; private set; }
        public int Height { get; private set; }
        public PixelFormat PixelFormatOfBuffer { get; private set; }
        private int GetIndex(int x, int y)
        {
            return x * bytesPerPixel + y * Stride;
        }
        public Color this[int x, int y]
        {
            get
            {
                int index = GetIndex(x, y);
                Color color = new Color();
                color.B = pixelData[index];
                color.G = pixelData[index + 1];
                color.R = pixelData[index + 2];
//                color.A = pixelData[index + 3];
                return color;
            }
            set
            {
                int index = GetIndex(x, y);
                pixelData[index] = value.B;
                pixelData[index + 1] = value.G;
                pixelData[index + 2] = value.R;
//                pixelData[index + 3] = value.A;
            }
        }
        public void Clean()
        {
            for (var i = 0; i < pixelData.Length; ++i)
                pixelData[i] = 0;
        }
        public BitmapSource CreateBitmapSource(double dpiX, double dpiY, BitmapPalette palette)
        {
            return BitmapSource.Create(Width, Height, dpiX, dpiY, PixelFormatOfBuffer, palette, pixelData, Stride);
        }
    }
}
