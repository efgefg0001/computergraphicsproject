﻿using System.Windows.Media;

namespace PhotonMappingApp
{
    
    public class Power
    {
        private Color color;
        private float wattage;
        private float red, green, blue; 
        public Power(Color color, float wattage)
        {
            Color = color;
            Wattage = wattage;
        }
        private void ComputeRGB()
        {
            float sum = color.ScR + color.ScG + color.ScB;
            red = sum == 0? 0 : color.ScR/sum * Wattage;
            green = sum == 0? 0 : color.ScG/sum * Wattage;
            blue = sum == 0? 0 : color.ScB/sum * Wattage;
        }
        public Color Color 
        {
            get { return color; }
            set 
            {
                color = value;
                ComputeRGB(); 
            } 
        }
        public float Wattage 
        {
            get { return wattage; }
            set 
            {
                wattage = value;
                ComputeRGB();
            }
        }
        public float Red { get { return red; } }
        public float Green { get { return green; } }
        public float Blue { get { return blue; } }
        public Power Copy()
        {
            var copyOfColor = new Color { ScA = color.ScA, ScR = color.ScR, ScG = color.ScR, ScB = color.ScB };
            var copyOfWattage = wattage;
            return new Power(copyOfColor, copyOfWattage);
        }
    }
    
} 