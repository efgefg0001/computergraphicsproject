﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace PhotonMappingApp
{
    public class Photon
    {
        public Photon(Point3D position, Vector3D dir, Power power)
        {
            Position = position;
            Direction = dir;
            Power = power;
            Plane = 0;
        }
        public Power Power { get; set; }
        public Point3D Position { get; set; }
        public Vector3D Direction { get; set; }
        public int Plane { get; set; }
    }
}
